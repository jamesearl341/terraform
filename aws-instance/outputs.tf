output "controller_node" {
  description = "ID of the controller node"
  value       = aws_instance.controller_node.id
}

output "controller_node_ip" {
  description = "Public IP address of the controller node"
  value       = aws_instance.controller_node.public_ip
}

output "worker_nodes" {
  description = "ID of the worker node"
  value       = aws_instance.worker_node.*.id
}

output "worker_nodes_ip" {
  description = "Public IP address of the worker node"
  value       = aws_instance.worker_node.*.public_ip
}
